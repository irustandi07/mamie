package main

func heat(obj *Obj) {
	obj.HeatedPan = true
	obj.Sequence = append(obj.Sequence, "Heat the Frying pan for #"+obj.OrderNumber)
}
