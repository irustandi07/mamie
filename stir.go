package main

func stir(obj *Obj) {
	obj.StiredNoodle = true
	obj.Sequence = append(obj.Sequence, "Stir the noodle #"+obj.OrderNumber)
}
