package main

import (
	"encoding/json"
	"net/http"
	"time"
)

type Obj struct {
	OrderNumber  string
	BoiledNoodle bool
	StiredNoodle bool
	HeatedPan    bool
	Fried        bool
	Sequence     []string
	ElapsedTime  string
}

func main() {
	http.HandleFunc("/", app)

	http.ListenAndServe(":9999", nil)
}

func app(w http.ResponseWriter, r *http.Request) {
	now := time.Now()

	var obj Obj

	obj.OrderNumber = now.Format("20060201150405.000000")

	boil(&obj)
	stir(&obj)
	heat(&obj)
	// go stir(&obj)
	// go heat(&obj)

	// var next = false

	// for !next {
	// 	time.Sleep(time.Nanosecond * 1)

	// 	if obj.HeatedPan && obj.StiredNoodle {
	// 		next = true
	// 	}
	// }

	fry(&obj)
	serve(&obj)

	elapsed := time.Since(now)
	obj.ElapsedTime = elapsed.String()

	test, _ := json.Marshal(&obj)

	w.Write([]byte(string(test)))

}
