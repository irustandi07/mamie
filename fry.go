package main

func fry(obj *Obj) {
	obj.Fried = true
	obj.Sequence = append(obj.Sequence, "Fry the noodle #"+obj.OrderNumber)
}
